﻿/*
MIT License

Copyright (c) 2017 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Core;

namespace Core.Data
{
    /// <summary>
    /// Generic repository.
    /// Should use this one if no custom method is nedded.
    /// </summary>
    internal class GenericRepository<T>: IGenericRepository<T>
        where T: BaseModel
    {
        /// <summary>
        /// The data context.
        /// </summary>
        private readonly DataContext<T> context;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Core.Data.GenericRepository`1"/> class.
        /// </summary>
        public GenericRepository()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OpenERP"].ConnectionString;
            context = new DataContext<T>(connectionString);
        }

        /// <summary>
        /// Find the object by the specified id.
        /// </summary>
        /// <returns>The find.</returns>
        /// <param name="id">Identifier.</param>
        public T Find(int id)
        {
            return context.ModelCollection.FirstOrDefault(a => a.Id.Equals(id));
        }

        /// <summary>
        /// Find all instances of a given entity.
        /// </summary>
        /// <returns>The collection of objects found.</returns>
        public List<T> Find()
        {
            return context.ModelCollection.ToList<T>();
        }

        /// <summary>
        /// Remove the specified obj from database.
        /// </summary>
        /// <param name="obj">Object to be removed.</param>
        public void Remove(T obj)
        {
            context.ModelCollection.Remove(Find(obj.Id));
            context.SaveChanges();
        }

        /// <summary>
        /// Save the specified obj.
        /// </summary>
        /// <returns>The Id of new created object, or the number of affected rows if object was updated.</returns>
        /// <param name="obj">Object to be saved/updated.</param>
        public int Save(T obj)
        {
            int idOrAffectedRows = 0;

            if (obj.Id.Equals(0))
            {
                T objFromDb = context.ModelCollection.Add(obj);
                idOrAffectedRows = objFromDb != null ? objFromDb.Id : 0;
            }
            else
            {
                T objFromDb = Find(obj.Id);

                foreach (var info in obj.GetType().GetProperties())
                {
                    // If property has public SET accessor.
                    if (info.GetSetMethod() != null)
                    {
                        // Sets the DB object with the value of the given object.
                        objFromDb.GetType().GetProperty(info.Name).SetValue(objFromDb, info.GetValue(obj));
                    }
                }
            }

            context.SaveChanges();

            return idOrAffectedRows;
        }
    }
}
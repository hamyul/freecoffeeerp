﻿/*
MIT License

Copyright (c) 2017 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Model;

namespace Core.Data
{
	/// <summary>
	/// Facade class to the database access.
	/// </summary>
	public class Facade
	{
		/// <summary>
		/// The branch repository.
		/// </summary>
		private readonly IGenericRepository<Branch> branchRepository;

		/// <summary>
		/// Default constructor.
		/// </summary>
		public Facade()
		{
			branchRepository = new GenericRepository<Branch>();
		}

		/// <summary>
		/// Save the specified obj.
		/// </summary>
		/// <returns>The save.</returns>
		/// <param name="obj">Object.</param>
		public int SaveBranch(Branch obj)
		{
			return branchRepository.Save(obj);
		}

		/// <summary>
		/// Remove the specified obj.
		/// </summary>
		/// <returns>The remove.</returns>
		/// <param name="obj">Object.</param>
		public void RemoveBranch(Branch obj)
		{
			branchRepository.Remove(obj);
		}

		/// <summary>
		/// Find the specified id.
		/// </summary>
		/// <returns>The find.</returns>
		/// <param name="id">Identifier.</param>
		public Branch FindBranch(int id)
		{
			return branchRepository.Find(id);
		}

		/// <summary>
		/// Find this instance.
		/// </summary>
		/// <returns>The find.</returns>
		public List<Branch> FindBranch()
		{
			return branchRepository.Find();
		}
	}
}
﻿/*
MIT License

Copyright (c) 2017 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;

namespace Core
{
    /// <summary>
    /// Base entity class.
    /// </summary>
	public abstract class BaseModel : IEquatable<BaseModel>
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to the current <see cref="T:Core.BaseModel"/>.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with the current <see cref="T:Core.BaseModel"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="object"/> is equal to the current <see cref="T:Core.BaseModel"/>;
        /// otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            return (obj as BaseModel).Id.Equals(Id);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Core.BaseModel"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Core.BaseModel"/>.</returns>
        public override string ToString()
        {
            return string.Format("[BaseModel: Id={0}]", Id);
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="T:Core.BaseModel"/> object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a
        /// hash table.</returns>
        public override int GetHashCode()
        {
			int hash = 13;
			hash = (hash * 7) + Id.GetHashCode();
			return hash;
        }

		/// <summary>
		/// Determines whether the specified <see cref="Core.BaseModel"/> is equal to the current <see cref="T:Core.BaseModel"/>.
		/// </summary>
		/// <param name="other">The <see cref="Core.BaseModel"/> to compare with the current <see cref="T:Core.BaseModel"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="Core.BaseModel"/> is equal to the current <see cref="T:Core.BaseModel"/>;
		/// otherwise, <c>false</c>.</returns>
		public bool Equals(BaseModel other)
		{
			return Id.Equals(other.Id);
		}
	}
}
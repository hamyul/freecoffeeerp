﻿/*
MIT License

Copyright (c) 2017 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;

namespace Model.Helpers
{
    /// <summary>
    /// Class that represents an address.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        /// <value>The street address.</value>
        public string StreetAddress { get; set; }

        /// <summary>
        /// Gets or sets the complement.
        /// </summary>
        /// <value>The complement.</value>
        public string Complement { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>The postal code.</value>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state of the province.
        /// </summary>
        /// <value>The state of the province.</value>
        public string ProvinceState { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Model.Helpers.Address"/> class.
        /// </summary>
        public Address()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Model.Helpers.Address"/> class.
        /// </summary>
        /// <param name="streetAddress">Street address.</param>
        /// <param name="complement">The address complement.</param>
        /// <param name="postalCode">Postal code.</param>
        /// <param name="city">City name.</param>
        /// <param name="provinceState">Province or state name.</param>
        /// <param name="country">Country.</param>
        public Address(string streetAddress, string complement, string postalCode, string city, string provinceState, string country)
        {
            StreetAddress = streetAddress;
            Complement = complement;
            PostalCode = postalCode;
            City = city;
            ProvinceState = provinceState;
            Country = country;
        }
    }
}

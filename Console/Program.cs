﻿using System;
using Core.Data;
using Model;
using Model.Helpers;

namespace OpenERP
{
	internal static class MainClass
	{
		public static void Main(string[] args)
		{
			Branch branch = new Branch();
			branch.Address = new Address { StreetAddress = "1600, rue Montgolfier", City = "Laval", Country = "Canada", ProvinceState = "QC", PostalCode = "H7T 0A2" };
			branch.Code = "001";
			branch.Name = "JBS Ltde";
			branch.Description = "This is a test";

			Facade fac = new Facade();
			fac.SaveBranch(branch);
		}
	}
}
